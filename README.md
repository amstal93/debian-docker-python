# debian-docker-python

A Docker image that contains just the basic tools to run molecule with docker.

## Building

Building is as easy as just running `make`, with tag being the branch name:

~~~bash
make
~~~

## Downloading

`debian-docker-python` can be downloaded from Docker Hub or GitLab:

~~~bash
docker pull julienlecomte/debian-docker-python
# or
docker pull registry.gitlab.com/jlecomte/images/debian-docker-python
~~~

## Contents

This docker inherits from [`debian:stable`](https://hub.docker.com/_/debian), and adds:
  * `docker`: to have be able to run Docker in Docker
  * `make`: to enable building via _Makefiles_

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Locations

  * GitLab: [https://gitlab.com/jlecomte/images/debian-docker-python](https://gitlab.com/jlecomte/images/docker-make)
  * Docker hub: [https://hub.docker.com/r/julienlecomte/debian-docker-python](https://hub.docker.com/r/julienlecomte/docker-make)

